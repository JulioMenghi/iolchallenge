﻿using CodingChallenge.Dominio.Formas;
using CodingChallenge.Dominio.Idiomas;
using CodingChallenge.Persistence;
using System.Collections.Generic;

namespace CodingChallenge.Compartido
{
    public static class Constantes
    {
        public static class Mensajes
        {
            public static string LineBreak = "<br/>";

            public static List<CustomMensaje> Castellano
            {
                get
                {
                    return new List<CustomMensaje>()
                    {
                        new CustomMensaje(TipoMensaje.Formas, "formas" ),
                        new CustomMensaje(TipoMensaje.ListaVacia, "<h1>Lista vacía de formas!</h1>"),
                        new CustomMensaje(TipoMensaje.Perimetro, "Perimetro"),
                        new CustomMensaje(TipoMensaje.ReporteDeFormas, "<h1>Reporte de Formas</h1>"),
                        new CustomMensaje(TipoMensaje.Area, "Area"),
                        new CustomMensaje(TipoMensaje.Total, "TOTAL"),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Cuadrado", typeof(Cuadrado)),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Triángulo", typeof(TrianguloEquilatero) ),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Círculo", typeof(Circulo)),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Rectángulo", typeof(Rectangulo)),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Trapecio", typeof(Trapecio))
                    };
                }
            }

            public static List<CustomMensaje> Ingles
            {
                get
                {
                    return new List<CustomMensaje>()
                    {
                        new CustomMensaje(TipoMensaje.Formas, "shapes" ),
                        new CustomMensaje(TipoMensaje.ListaVacia, "<h1>Empty list of shapes!</h1>"),
                        new CustomMensaje(TipoMensaje.Perimetro, "Perimeter"),
                        new CustomMensaje(TipoMensaje.ReporteDeFormas, "<h1>Shapes report</h1>"),
                        new CustomMensaje(TipoMensaje.Area, "Area"),
                        new CustomMensaje(TipoMensaje.Total, "TOTAL"),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Square", typeof(Cuadrado)),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Triangle", typeof(TrianguloEquilatero)),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Circle", typeof(Circulo)),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Rectangle", typeof(Rectangulo)),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Trapeze", typeof(Trapecio))
                    };
                }
            }

            public static List<CustomMensaje> Portugues
            {
                get
                {
                    return new List<CustomMensaje>()
                    {
                        new CustomMensaje(TipoMensaje.Formas, "formas" ),
                        new CustomMensaje(TipoMensaje.ListaVacia, "<h1>Lista vazia de formas!</h1>"),
                        new CustomMensaje(TipoMensaje.Perimetro, "Perimetro"),
                        new CustomMensaje(TipoMensaje.ReporteDeFormas, "<h1>Relatório de forma</h1>"),
                        new CustomMensaje(TipoMensaje.Area, "Area"),
                        new CustomMensaje(TipoMensaje.Total, "TOTAL"),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Quadrado", typeof(Cuadrado)),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Triângulo", typeof(Cuadrado)),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Círculo", typeof(Circulo)),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Retângulo", typeof(Rectangulo)),
                        new CustomMensaje(TipoMensaje.NombreFigura, "Trapézio", typeof(Trapecio))
                    };
                }
            }
        }
    }
}
