
# InvertirOnline.com Coding Challenge
### El problema

Tenemos un método que genera un reporte en base a una colección de formas geométricas, procesando algunos datos para presentar información extra. La firma del método es:

```csharp
public static string Imprimir(List<FormaGeometrica> formas, int idioma)
```

Al mismo tiempo, encontramos muy díficil el poder agregar o bien una nueva forma geométrica, o imprimir el reporte en otro idioma. Nos gustaría poder dar soporte para que el usuario pueda agregar otros tipos de formas u obtener el reporte en otros idiomas, pero extender la funcionalidad del código es muy doloroso. ¿Nos podrías dar una mano a refactorear la clase FormaGeometrica? Dentro del código encontrarás un TODO con nuevos requerimientos a satisfacer una vez completada la refactorización.

Acompañando al proyecto encontrarás una serie de tests unitarios (librería NUnit) que describen el comportamiento del método Imprimir. **Se puede modificar cualquier cosa del código y de los tests, con la única condición que los tests deben pasar correctamente al entregar la solución.** 

Se agradece también la inclusión de nuevos tests unitarios para validar el comportamiento de la nueva funcionalidad agregada.

### Solución Aplicada

Se hicieron extracciones de clases, metodos y responsabilidades que estaban de mas en la clase FormaGeometrica. Se mantuvo el nombre pero transformada en un Factory generico (T) el cual instancia el tipo indicado.
Las caracteristicas comunes (Area, Perimetro, etc) de las diferentes formas geometricas (Cuadrado, Circulo, etc) se abstrayeron en interfaces que las clases concretas implementan junto a sus funcionamientos propios (Calculos de area por ejemplo).
Asi para agregar una nueva forma geometrica solo es necesario crear la clase e implementar la(s) interface(s) adecuada(s).

Por el lado de los idiomas, tambien opte por usar un Factory, el cual instancia una clase Idioma la cual hereda de una clase abstracta y contiene una lista de mensajes y algunos metodos de busqueda. El factory recibe el lenguaje proveniente de un enum (para persistencia) y segun eso instancia el Idioma con los mensajes correspondientes.
Para agregar un nuevo idioma simplemente se debe agregarlo en el enum, agregar las traducciones en el archivo de Constantes y extender la logica en el factory. 

Y por el lado del Reporting, se creo una clase Impresora que ejecuta un metodo "Imprimir" el cual recibe simplemente la coleccion y el idioma para trabajar y devolver el resumen.

Respecto a los tests se mantuvieron los antiguos pero con la nueva implementacion.
Se agregaron tests correspondientes a las nuevas funcionalidades y clases.

