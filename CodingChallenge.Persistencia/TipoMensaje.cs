﻿namespace CodingChallenge.Persistence
{
    public enum TipoMensaje
    {
        ListaVacia = 1,
        ReporteDeFormas = 2,
        Formas = 3,
        Perimetro = 4,
        NombreFigura = 5,
        Area = 6,
        Total = 7
    }
}
