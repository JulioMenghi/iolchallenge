﻿using CodingChallenge.Compartido;
using CodingChallenge.Dominio.Idiomas;
using CodingChallenge.Persistence;
using System.Collections.Generic;

namespace CodingChallenge.Infraestructura.Factory
{
    public static class IdiomaManager
    {
        public static Idioma ConfigurarIdioma(Idiomas idioma)
        {
            IEnumerable<CustomMensaje> mensajes;

            switch(idioma)
            {
                case Idiomas.Castellano:
                    mensajes = Constantes.Mensajes.Castellano;
                    break;

                case Idiomas.Ingles:
                    mensajes = Constantes.Mensajes.Ingles;
                    break;

                case Idiomas.Portugues:
                    mensajes = Constantes.Mensajes.Portugues;
                    break;

                    //agregar nuevos idiomas a implementar

                default:
                    mensajes = Constantes.Mensajes.Castellano;
                    break;                                     
            }
            return new Idioma(mensajes);
        }
    }
}
