﻿using CodingChallenge.Compartido;
using CodingChallenge.Dominio.Interfaces;
using CodingChallenge.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodingChallenge.Infraestructura.Reporting
{
    public static class Impresora
    {
        public static string Imprimir(List<IFormaGeometrica> formas, IdiomaBase idioma)
        {
            var sb = new StringBuilder();

            if (!formas.Any())
            {
                return idioma.BuscarMensajeEspecifico(TipoMensaje.ListaVacia);
            }
            else
            {
                // Hay por lo menos una forma
                // HEADER
                sb.Append(idioma.BuscarMensajeEspecifico(TipoMensaje.ReporteDeFormas));

                var types = AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(s => s.GetTypes())
                    .Where(p => typeof(IFormaGeometrica).IsAssignableFrom(p));

                var perimetroTotal = 0m;
                var areaTotal = 0m;
                foreach (var tipo in types)
                {
                    var formasPorTipo = formas.Where(x => x.GetType() == tipo).ToList();

                    if (formasPorTipo.Any())
                    {
                        var totalFormasPorTipo = formasPorTipo.Count;
                        var areaTotalPorTipo = formasPorTipo.Sum(x => x.CalcularArea()); ;
                        var perimetroTotalPorTipo = formasPorTipo.Sum(x => x.CalcularPerimetro());

                        perimetroTotal += perimetroTotalPorTipo;
                        areaTotal += areaTotalPorTipo;

                        sb.Append(ObtenerLineaSegunTipoForma(totalFormasPorTipo, areaTotalPorTipo, perimetroTotalPorTipo, tipo, idioma));
                    }
                }

                // FOOTER
                sb.Append($"{idioma.BuscarMensajeEspecifico(TipoMensaje.Total)}:{Constantes.Mensajes.LineBreak}");
                sb.Append($"{formas.Count} {idioma.BuscarMensajeEspecifico(TipoMensaje.Formas)} ");
                sb.Append($"{idioma.BuscarMensajeEspecifico(TipoMensaje.Perimetro)} {perimetroTotal.ToString("#.##")} ");
                sb.Append($"{idioma.BuscarMensajeEspecifico(TipoMensaje.Area)} {areaTotal.ToString("#.##")}");
            }

            return sb.ToString();
        }

        private static string ObtenerLineaSegunTipoForma(int totalFormas, decimal totalArea, decimal totalPerimetro, Type tipo, IdiomaBase idioma)
        {
            var areaLabel = idioma.BuscarMensajeEspecifico(TipoMensaje.Area);
            var perimetroLabel = idioma.BuscarMensajeEspecifico(TipoMensaje.Perimetro);
            var formaSegunCantidadLabel = idioma.BuscarMensajeEspecifico(TipoMensaje.NombreFigura, tipo);
            formaSegunCantidadLabel = totalFormas > 1 ? formaSegunCantidadLabel + "s" : formaSegunCantidadLabel;

            return $"{totalFormas} {formaSegunCantidadLabel} | {areaLabel} {totalArea:#.##} | {perimetroLabel} {totalPerimetro:#.##} <br/>"; 
        }
    }
}
