﻿using CodingChallenge.Dominio.Formas;
using CodingChallenge.Infraestructura.Factory;
using NUnit.Framework;
using System;

namespace CodingChallenge.UnitTests.Dominio.Formas
{
    public class TrianguloEquilateroTests
    {
        [Datapoints]
        readonly decimal[] medidasLados = new decimal[] { 5, -3, 12 };

        [Theory]
        public void CalcularArea_MedidaLadoTieneValor_DevuelveCalculoOk(decimal medidaLado)
        {
            //Arrange
            object[] values = { medidaLado };
            var triangulo = FormaGeometrica.CrearNuevaForma<TrianguloEquilatero>(values);

            //Act
            var resultado = triangulo.CalcularArea();

            //Assert
            Assert.NotNull(resultado);
            Assert.Greater(resultado, 0);

            var areaCalculoOk = ((decimal)Math.Sqrt(3) / 4) * triangulo.MedidaLado * triangulo.MedidaLado;
            Assert.AreEqual(resultado, areaCalculoOk);
        }

        [Theory]
        public void CalcularPerimetro_MedidaLadoTieneValor_DevuelveCalculoOk(decimal medidaLado)
        {
            //Arrange
            object[] values = { medidaLado };
            var triangulo = FormaGeometrica.CrearNuevaForma<TrianguloEquilatero>(values);

            //Act
            var resultado = triangulo.CalcularPerimetro();

            //Assert
            Assert.NotNull(resultado);
            Assert.Greater(resultado, 0);
            Assert.AreEqual(resultado, triangulo.MedidaLado * 3);
        }
    }
}
