﻿using CodingChallenge.Dominio.Formas;
using CodingChallenge.Infraestructura.Factory;
using NUnit.Framework;
using System;

namespace CodingChallenge.UnitTests.Dominio.Formas
{
    public class CirculoTests
    {
        [Datapoints]
        readonly decimal[] medidasLados = new decimal[] { 5, -3, 12 };

        [Theory]
        public void CalcularArea_MedidaLadoTieneValor_DevuelveCalculoOk(decimal medidaLado)
        {
            //Arrange
            object[] values = { medidaLado };
            var circulo = FormaGeometrica.CrearNuevaForma<Circulo>(values);

            //Act
            var resultado = circulo.CalcularArea();

            //Assert
            Assert.NotNull(resultado);
            Assert.Greater(resultado, 0);

            var areaCalculoOk = (decimal)Math.PI * (circulo.MedidaLado / 2) * (circulo.MedidaLado / 2);
            Assert.AreEqual(resultado, areaCalculoOk);
        }

        [Theory]
        public void CalcularPerimetro_MedidaLadoTieneValor_DevuelveCalculoOk(decimal medidaLado)
        {
            //Arrange
            object[] values = { medidaLado };
            var circulo = FormaGeometrica.CrearNuevaForma<Circulo>(values);

            //Act
            var resultado = circulo.CalcularPerimetro();

            //Assert
            Assert.NotNull(resultado);
            Assert.Greater(resultado, 0);
            Assert.AreEqual(resultado, (decimal)Math.PI * circulo.MedidaLado);
        }
    }
}