﻿using CodingChallenge.Dominio.Formas;
using CodingChallenge.Infraestructura.Factory;
using NUnit.Framework;

namespace CodingChallenge.UnitTests.Dominio.Formas
{
    public class RectanguloTests
    {
        [Datapoints]
        readonly decimal[] medidaLados = new decimal[] { 5, -11 };

        [Theory]
        public void CalcularArea_BaseYAlturaTienenValor_DevuelveCalculoOk(decimal _base, decimal altura)
        {
            //Arrange
            object[] values = { _base, altura };
            var rectangulo = FormaGeometrica.CrearNuevaForma<Rectangulo>(values);

            //Act
            var resultado = rectangulo.CalcularArea();

            //Assert
            Assert.NotNull(resultado);
            Assert.Greater(resultado, 0);
            Assert.AreEqual(resultado, rectangulo.Base * rectangulo.Altura);
        }

        [Theory]
        public void CalcularPerimetro_BaseYAlturaTienenValor_DevuelveCalculoOk(decimal _base, decimal altura)
        {
            //Arrange
            object[] values = { _base, altura };
            var rectangulo = FormaGeometrica.CrearNuevaForma<Rectangulo>(values);

            //Act
            var resultado = rectangulo.CalcularPerimetro();

            //Assert
            Assert.NotNull(resultado);
            Assert.Greater(resultado, 0);

            var perimetroCalculoOk = (rectangulo.Base * 2) + (rectangulo.Altura * 2);
            Assert.AreEqual(resultado, perimetroCalculoOk);
        }
    }
}
