﻿using CodingChallenge.Dominio.Formas;
using CodingChallenge.Infraestructura.Factory;
using NUnit.Framework;

namespace CodingChallenge.UnitTests.Dominio.Formas
{
    public class CuadradoTests
    {
        [Datapoints]
        readonly decimal[] medidasLados = new decimal[] { 5, -3, 12 };

        [Theory]
        public void CalcularArea_MedidaLadoTieneValor_DevuelveCalculoOk(decimal medidaLado)
        {
            //Arrange
            object[] values = { medidaLado };
            var cuadrado = FormaGeometrica.CrearNuevaForma<Cuadrado>(values);

            //Act
            var resultado = cuadrado.CalcularArea();

            //Assert
            Assert.NotNull(resultado);
            Assert.Greater(resultado, 0);
            Assert.AreEqual(resultado, cuadrado.MedidaLado * cuadrado.MedidaLado);
        }

        [Theory]
        public void CalcularPerimetro_MedidaLadoTieneValor_DevuelveCalculoOk(decimal medidaLado)
        {
            //Arrange
            object[] values = { medidaLado };
            var cuadrado = FormaGeometrica.CrearNuevaForma<Cuadrado>(values);

            //Act
            var resultado = cuadrado.CalcularPerimetro();

            //Assert
            Assert.NotNull(resultado);
            Assert.Greater(resultado, 0);
            Assert.AreEqual(resultado, cuadrado.MedidaLado * 4);
        }
    }
}
