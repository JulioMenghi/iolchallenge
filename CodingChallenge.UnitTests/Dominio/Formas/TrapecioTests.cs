﻿using CodingChallenge.Dominio.Formas;
using CodingChallenge.Infraestructura.Factory;
using NUnit.Framework;
using System;

namespace CodingChallenge.UnitTests.Dominio.Formas
{
    public class TrapecioTests
    {
        private static readonly Random generador = new Random();

        [TestCase]
        public void CalcularArea_LadosTienenValor_DevuelveCalculoOk()

        {
            //Arrange
            object[] values =
                {
                    (decimal)generador.Next(-10, 9999),
                    (decimal)generador.Next(-10, 9999),
                    (decimal)generador.Next(-10, 9999),
                    (decimal)generador.Next(-10, 9999),
                    (decimal)generador.Next(-10, 9999),
                };

            var trapecio = FormaGeometrica.CrearNuevaForma<Trapecio>(values);

            //Act
            var resultado = trapecio.CalcularArea();

            //Assert
            Assert.NotNull(resultado);
            Assert.Greater(resultado, 0);
            Assert.AreEqual(resultado, trapecio.Altura * ((trapecio.Base1 + trapecio.Base2) / 2));
        }

        [TestCase]
        public void CalcularPerimetro_LadosTienenValor_DevuelveCalculoOk()
        {
            //Arrange
            object[] values =
                {
                    (decimal)generador.Next(-10, 9999),
                    (decimal)generador.Next(-10, 9999),
                    (decimal)generador.Next(-10, 9999),
                    (decimal)generador.Next(-10, 9999),
                    (decimal)generador.Next(-10, 9999),
                };

            var trapecio = FormaGeometrica.CrearNuevaForma<Trapecio>(values);

            //Act
            var resultado = trapecio.CalcularPerimetro();

            //Assert
            Assert.NotNull(resultado);
            Assert.Greater(resultado, 0);

            var perimetroCalculoOk = trapecio.Base1 + trapecio.Base2 + trapecio.Lado3 + trapecio.Lado4;
            Assert.AreEqual(resultado, perimetroCalculoOk);
        }
    }
}
