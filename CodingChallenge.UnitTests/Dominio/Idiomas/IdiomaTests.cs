﻿using CodingChallenge.Dominio.Formas;
using CodingChallenge.Infraestructura.Factory;
using CodingChallenge.Persistence;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace CodingChallenge.UnitTests.Dominio.Idiomas
{
    public class IdiomaTests
    {
        private Random generador = new Random();

        [TestCase]
        public void BuscarMensajeEspecifico_TipoMensajeEnviados_DevuelveMensaje()
        {
            //Arrange
            var totalIdiomas = Enum.GetNames(typeof(Persistence.Idiomas)).Length;
            var idiomaId = (Persistence.Idiomas)generador.Next(1, totalIdiomas - 1);
            var idioma = IdiomaManager.ConfigurarIdioma(idiomaId);

            var totalMensajes = Enum.GetNames(typeof(Persistence.Idiomas)).Length;
            var tipoMensaje = (TipoMensaje)generador.Next(1, totalMensajes - 1);

            //Act
            var mensaje = idioma.BuscarMensajeEspecifico(tipoMensaje);

            //Assert
            Assert.NotNull(mensaje);
            Assert.IsNotEmpty(mensaje);
            Assert.IsAssignableFrom<string>(mensaje);
        }

        [TestCase]
        public void BuscarMensajeEspecifico_TipoMensajeYFiltroEnviados_DevuelveMensaje()
        {
            //Arrange
            var totalIdiomas = Enum.GetNames(typeof(Persistence.Idiomas)).Length;
            var idiomaId = (Persistence.Idiomas)generador.Next(1, totalIdiomas - 1);
            var idioma = IdiomaManager.ConfigurarIdioma(idiomaId);

            var totalMensajes = Enum.GetNames(typeof(Persistence.Idiomas)).Length;
            var tipoMensaje = (TipoMensaje)generador.Next(1, totalMensajes - 1);

            //Act
            var mensaje = idioma.BuscarMensajeEspecifico(tipoMensaje, string.Empty);

            //Assert
            Assert.NotNull(mensaje);
            Assert.IsNotEmpty(mensaje);
            Assert.IsAssignableFrom<string>(mensaje);
        }

        [TestCase]
        public void BuscarMensajeEspecifico_TipoMensajeYTipoClaseEnviados_DevuelveMensaje()
        {
            //Arrange
            var totalIdiomas = Enum.GetNames(typeof(Persistence.Idiomas)).Length;
            var idiomaId = (Persistence.Idiomas)generador.Next(1, totalIdiomas - 1);
            var idioma = IdiomaManager.ConfigurarIdioma(idiomaId);

            var tipoMensaje = TipoMensaje.NombreFigura;
            var tipoClase = typeof(Trapecio);

            //Act
            var mensaje = idioma.BuscarMensajeEspecifico(tipoMensaje, tipoClase);

            //Assert
            Assert.NotNull(mensaje);
            Assert.IsNotEmpty(mensaje);
            Assert.IsAssignableFrom<string>(mensaje);
        }

        [TestCase]
        public void BuscarMensajes_TipoMensajeEnviados_DevuelveListaMensajes()
        {
            //Arrange
            var totalIdiomas = Enum.GetNames(typeof(Persistence.Idiomas)).Length;
            var idiomaId = (Persistence.Idiomas)generador.Next(1, totalIdiomas - 1);
            var idioma = IdiomaManager.ConfigurarIdioma(idiomaId);

            var totalMensajes = Enum.GetNames(typeof(Persistence.Idiomas)).Length;
            var tipoMensaje = (TipoMensaje)generador.Next(1, totalMensajes - 1);

            //Act
            var listaMensajes = idioma.BuscarMensajes(tipoMensaje);

            //Assert
            Assert.NotNull(listaMensajes);
            Assert.IsNotEmpty(listaMensajes);
            Assert.IsAssignableFrom<List<string>>(listaMensajes);
        }

        [TestCase]
        public void BuscarMensajes_FiltroEnviado_DevuelveListaMensajes()
        {
            //Arrange
            var totalIdiomas = Enum.GetNames(typeof(Persistence.Idiomas)).Length;
            var idiomaId = (Persistence.Idiomas)generador.Next(1, totalIdiomas - 1);
            var idioma = IdiomaManager.ConfigurarIdioma(idiomaId);

            //Act
            var listaMensajes = idioma.BuscarMensajes(string.Empty);

            //Assert
            Assert.NotNull(listaMensajes);
            Assert.IsNotEmpty(listaMensajes);
            Assert.IsAssignableFrom<List<string>>(listaMensajes);
        }

        [TestCase]
        public void BuscarMensajes_TipoClaseEnviado_DevuelveListaMensajes()
        {
            //Arrange
            var totalIdiomas = Enum.GetNames(typeof(Persistence.Idiomas)).Length;
            var idiomaId = (Persistence.Idiomas)generador.Next(1, totalIdiomas - 1);
            var idioma = IdiomaManager.ConfigurarIdioma(idiomaId);

            var tipoClase = typeof(TrianguloEquilatero);

            //Act
            var listaMensajes = idioma.BuscarMensajes(tipoClase);

            //Assert
            Assert.NotNull(listaMensajes);
            Assert.IsNotEmpty(listaMensajes);
            Assert.IsAssignableFrom<List<string>>(listaMensajes);
        }
    }
}
