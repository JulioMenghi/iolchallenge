﻿using CodingChallenge.Dominio.Formas;
using CodingChallenge.Dominio.Interfaces;
using CodingChallenge.Infraestructura.Factory;
using CodingChallenge.Infraestructura.Reporting;
using CodingChallenge.Persistence;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace CodingChallenge.UnitTests.Infraestructura.Reporting
{
    public class ImpresoraTests
    {
        private Random generador = new Random();

        [TestCase]
        public void Imprimir_ListaFormasVacia_DevuelveMensajeCorrespondiente()
        {
            //Arrange
            var listaVacia = new List<IFormaGeometrica>();

            var totalIdiomas = Enum.GetNames(typeof(Persistence.Idiomas)).Length;
            var idiomaId = (Idiomas)generador.Next(1, totalIdiomas - 1);
            var idiomaAlAzar = IdiomaManager.ConfigurarIdioma(idiomaId);

            //Act
            var resultado = Impresora.Imprimir(listaVacia, idiomaAlAzar);

            //Assert
            Assert.NotNull(resultado);
            Assert.IsNotEmpty(resultado);
            Assert.IsAssignableFrom<string>(resultado);
            Assert.AreEqual(resultado,
                idiomaAlAzar.BuscarMensajeEspecifico(TipoMensaje.ListaVacia));
        }

        [TestCase]
        public void Imprimir_ListaFormasConElementos_DevuelveNombresFigurasCorrespondientes()
        {
            //Arrange
            object[] trianguloValues = { (decimal)12, };
            var triangulo = FormaGeometrica.CrearNuevaForma<TrianguloEquilatero>(trianguloValues);

            object[] triangulo2Values = { (decimal)17, };
            var triangulo2 = FormaGeometrica.CrearNuevaForma<TrianguloEquilatero>(triangulo2Values);

            object[] circuloValues = { (decimal)13 };
            var circulo = FormaGeometrica.CrearNuevaForma<Circulo>(circuloValues);

            var listaFormas = new List<IFormaGeometrica>()
            {
                triangulo, triangulo2, circulo
            };

            var totalIdiomas = Enum.GetNames(typeof(Persistence.Idiomas)).Length;
            var idiomaId = (Idiomas)generador.Next(1, totalIdiomas - 1);
            var idiomaAlAzar = IdiomaManager.ConfigurarIdioma(idiomaId);

            //Act
            var resultado = Impresora.Imprimir(listaFormas, idiomaAlAzar);

            //Assert
            Assert.NotNull(resultado);
            Assert.IsNotEmpty(resultado);
            Assert.IsAssignableFrom<string>(resultado);

            var nombreTriangulo = idiomaAlAzar.BuscarMensajeEspecifico(TipoMensaje.NombreFigura, typeof(TrianguloEquilatero));
            var nombreCirculo = idiomaAlAzar.BuscarMensajeEspecifico(TipoMensaje.NombreFigura, typeof(Circulo));

            StringAssert.Contains(nombreCirculo, resultado);
            StringAssert.Contains(nombreTriangulo + "s", resultado); //plural
        }

        [TestCase]
        public void Imprimir_ListaFormasConElementos_DevuelveResumenCorrespondiente()
        {
            //Arrange
            object[] cuadradoValues = { (decimal)5 }; //a 25, p 20
            var cuadrado = FormaGeometrica.CrearNuevaForma<Cuadrado>(cuadradoValues);

            object[] rectanguloValues = { (decimal)12, (decimal)-5 }; //a 12, p 26
            var rectangulo = FormaGeometrica.CrearNuevaForma<Rectangulo>(rectanguloValues);

            object[] trapecioValues = //a 47.5, p 31
                { (decimal)5, (decimal)15, (decimal)4, (decimal)-8, (decimal)11 };
            var trapecio = FormaGeometrica.CrearNuevaForma<Trapecio>(trapecioValues);

            var listaFormas = new List<IFormaGeometrica>()
            {
                cuadrado, rectangulo, trapecio
            };

            var totalIdiomas = Enum.GetNames(typeof(Idiomas)).Length;
            var idiomaId = (Idiomas)generador.Next(1, totalIdiomas - 1);
            var idiomaAlAzar = IdiomaManager.ConfigurarIdioma(idiomaId);

            //Act
            var resultado = Impresora.Imprimir(listaFormas, idiomaAlAzar);

            //Assert
            Assert.NotNull(resultado);
            Assert.IsNotEmpty(resultado);
            Assert.IsAssignableFrom<string>(resultado);

            var nombreCuadrado = idiomaAlAzar.BuscarMensajeEspecifico(TipoMensaje.NombreFigura, typeof(Cuadrado));
            var nombreRectangulo = idiomaAlAzar.BuscarMensajeEspecifico(TipoMensaje.NombreFigura, typeof(Rectangulo));
            var nombreTrapecio = idiomaAlAzar.BuscarMensajeEspecifico(TipoMensaje.NombreFigura, typeof(Trapecio));

            var cuadradoPerimetro = cuadrado.CalcularPerimetro();
            var cuadradoArea = cuadrado.CalcularArea();

            var rectanguloPerimetro = rectangulo.CalcularPerimetro();
            var rectanguloArea = rectangulo.CalcularArea();

            var trapecioPerimetro = trapecio.CalcularPerimetro();
            var trapecioArea = trapecio.CalcularArea();

            var areaTotal = (cuadradoArea + rectanguloArea + trapecioArea).ToString("#.##");

            var perimetroTotal = (cuadradoPerimetro + rectanguloPerimetro + trapecioPerimetro).ToString("#.##");
            var totalFormas = listaFormas.Count.ToString();

            StringAssert.Contains(nombreCuadrado, resultado);
            StringAssert.Contains(nombreRectangulo, resultado);
            StringAssert.Contains(nombreTrapecio, resultado);

            StringAssert.Contains(cuadradoPerimetro.ToString(), resultado);
            StringAssert.Contains(cuadradoArea.ToString(), resultado);
            StringAssert.Contains(rectanguloPerimetro.ToString(), resultado);
            StringAssert.Contains(rectanguloArea.ToString(), resultado);
            StringAssert.Contains(trapecioPerimetro.ToString(), resultado);
            StringAssert.Contains(trapecioArea.ToString(), resultado);
            StringAssert.Contains(areaTotal, resultado);
            StringAssert.Contains(perimetroTotal, resultado);
            StringAssert.Contains(totalFormas, resultado);
        }
    }
}
