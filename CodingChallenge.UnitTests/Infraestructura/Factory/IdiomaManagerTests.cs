﻿using CodingChallenge.Dominio.Idiomas;
using CodingChallenge.Infraestructura.Factory;
using CodingChallenge.Persistence;
using NUnit.Framework;

namespace CodingChallenge.UnitTests.Infraestructura.Factory
{
    public class IdiomaManagerTests
    {
        [TestCase]
        public void ConfigurarIdioma_IdiomaImplementadoEnviado_DevuelveIdiomaOk()
        {
            //Arrange
            var tipoIdioma = Idiomas.Ingles;

            //Act
            var idioma = IdiomaManager.ConfigurarIdioma(tipoIdioma);

            //Assert
            Assert.NotNull(idioma);
            Assert.IsNotEmpty(idioma.Mensajes);
            Assert.IsAssignableFrom<Idioma>(idioma);
        }

        [TestCase]
        public void ConfigurarIdioma_IdiomaNoImplementadoEnviado_DevuelveIdiomaPorDefecto()
        {
            //Arrange
            var tipoIdioma = Idiomas.Frances; //no implementado

            //Act
            var idioma = IdiomaManager.ConfigurarIdioma(tipoIdioma);

            //Assert
            Assert.NotNull(idioma);
            Assert.IsNotEmpty(idioma.Mensajes);
            Assert.IsAssignableFrom<Idioma>(idioma);
        }
    }
}
