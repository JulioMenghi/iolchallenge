﻿using CodingChallenge.Dominio.Formas;
using CodingChallenge.Infraestructura.Factory;
using NUnit.Framework;

namespace CodingChallenge.UnitTests.Infraestructura.Factory
{
    public class FormaGeometricaTests
    {
        [TestCase]
        public void CrearNuevaForma_ParametrosCorrectosEnviados_DevuelveNuevaForma()
        {
            //Arrange
            object[] values = { (decimal)3, (decimal)77 }; //puede ser x tipo 

            //Act
            var rectangulo = FormaGeometrica.CrearNuevaForma<Rectangulo>(values);

            //Assert
            Assert.NotNull(rectangulo);
            Assert.IsAssignableFrom<Rectangulo>(rectangulo);
            Assert.AreEqual(rectangulo.Base, values[0]);
            Assert.AreEqual(rectangulo.Altura, values[1]);
        }
    }
}
