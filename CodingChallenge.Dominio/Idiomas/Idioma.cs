﻿using CodingChallenge.Dominio.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace CodingChallenge.Dominio.Idiomas
{
    public class Idioma : IdiomaBase
    {
        public Idioma(IEnumerable<CustomMensaje> mensajes)
        {
            Mensajes = mensajes.ToList();
        }
    }
}
