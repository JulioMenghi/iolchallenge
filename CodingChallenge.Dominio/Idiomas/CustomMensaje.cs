﻿using CodingChallenge.Persistence;
using System;

namespace CodingChallenge.Dominio.Idiomas
{
    public class CustomMensaje
    {
        public CustomMensaje(TipoMensaje tipo, string mensaje, Type tipoClase = null)
        {
            Tipo = tipo;
            Mensaje = mensaje;
            TipoClase = tipoClase;
        }

        public string Mensaje { get; }
        public TipoMensaje Tipo { get; }
        public Type TipoClase { get; }
    }
}
