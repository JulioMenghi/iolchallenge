﻿using CodingChallenge.Dominio.Interfaces;
using System;

namespace CodingChallenge.Dominio.Formas
{
    public class TrianguloEquilatero : IEquilatero
    {
        public TrianguloEquilatero(decimal medidaLado)
        {
            MedidaLado = medidaLado;
        }

        private decimal _medidaLado;
        public decimal MedidaLado {
            get { return _medidaLado; }
            private set
            {
                if (value <= 0)
                {
                    _medidaLado = 1;
                }
                else
                {
                    _medidaLado = value;
                }
            }
        }

        public decimal CalcularArea()
        {
            return ((decimal)Math.Sqrt(3) / 4) * MedidaLado * MedidaLado;
        }

        public decimal CalcularPerimetro()
        {
            return MedidaLado * 3;
        }
    }
}
