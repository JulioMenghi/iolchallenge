﻿using CodingChallenge.Dominio.Interfaces;
using System;

namespace CodingChallenge.Dominio.Formas
{
    public class Circulo : IEquilatero
    {
        public Circulo(decimal medidaLado)
        {
            MedidaLado = medidaLado;
        }

        private decimal _medidaLado;
        public decimal MedidaLado
        {
            get { return _medidaLado; }
            private set
            {
                if (value <= 0)
                {
                    _medidaLado = 1;
                }
                else
                {
                    _medidaLado = value;
                }
            }
        }

        public decimal CalcularArea()
        {
            return (decimal)Math.PI * (MedidaLado / 2) * (MedidaLado / 2);
        }

        public decimal CalcularPerimetro()
        {
            return (decimal)Math.PI * MedidaLado;
        }
    }
}
