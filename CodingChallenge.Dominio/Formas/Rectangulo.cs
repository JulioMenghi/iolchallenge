﻿using CodingChallenge.Dominio.Interfaces;

namespace CodingChallenge.Dominio.Formas
{
    public class Rectangulo : IRectangulo
    {
        public Rectangulo(decimal _base, decimal altura)
        {
            Base = _base;
            Altura = altura;
        }

        private decimal _base;
        public decimal Base
        {
            get { return _base; }
            private set
            {
                if (value <= 0)
                {
                    _base = 1;
                }
                else
                {
                    _base = value;
                }
            }
        }

        private decimal _altura;
        public decimal Altura
        {
            get { return _altura; }
            private set
            {
                if (value <= 0)
                {
                    _altura = 1;
                }
                else
                {
                    _altura = value;
                }
            }
        }

        public decimal CalcularArea()
        {
            return Base * Altura;
        }

        public decimal CalcularPerimetro()
        {
            return (Base * 2) + (Altura * 2);
        }
    }
}
