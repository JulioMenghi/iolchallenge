﻿using CodingChallenge.Dominio.Interfaces;

namespace CodingChallenge.Dominio.Formas
{
    public class Trapecio : ITrapecio
    {
        public Trapecio(decimal altura, decimal base1, decimal base2, decimal lado3, decimal lado4)
        {
            Altura = altura;
            Base1 = base1;
            Base2 = base2;
            Lado3 = lado3;
            Lado4 = lado4;
        }
        private decimal _altura;
        public decimal Altura
        {
            get { return _altura; }
            private set
            {
                if (value <= 0)
                {
                    _altura = 1;
                }
                else
                {
                    _altura = value;
                }
            }
        }

        private decimal _base1;
        public decimal Base1
        {
            get { return _base1; }
            private set
            {
                if (value <= 0)
                {
                    _base1 = 1;
                }
                else
                {
                    _base1 = value;
                }
            }
        }

        private decimal _base2;
        public decimal Base2
        {
            get { return _base2; }
            private set
            {
                if (value <= 0)
                {
                    _base2 = 1;
                }
                else
                {
                    _base2 = value;
                }
            }
        }

        private decimal _lado3;
        public decimal Lado3
        {
            get { return _lado3; }
            private set
            {
                if (value <= 0)
                {
                    _lado3 = 1;
                }
                else
                {
                    _lado3 = value;
                }
            }
        }

        private decimal _lado4;
        public decimal Lado4
        {
            get { return _lado4; }
            private set
            {
                if (value <= 0)
                {
                    _lado4 = 1;
                }
                else
                {
                    _lado4 = value;
                }
            }
        }

        public decimal CalcularArea()
        {
            return Altura * ((Base1 + Base2) / 2);
        }

        public decimal CalcularPerimetro()
        {
            return Base1 + Base2 + Lado3 + Lado4;
        }
    }
}
