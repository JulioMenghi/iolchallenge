﻿using CodingChallenge.Dominio.Interfaces;

namespace CodingChallenge.Dominio.Formas
{
    public class Cuadrado : IEquilatero
    {
        public Cuadrado(decimal medidaLado)
        {
            MedidaLado = medidaLado;
        }

        private decimal _medidaLado;
        public decimal MedidaLado
        {
            get { return _medidaLado; }
            private set
            {
                if(value <= 0)
                {
                    _medidaLado = 1;
                }
                else
                {
                    _medidaLado = value;
                }
            }
        }

        public decimal CalcularArea()
        {
            return MedidaLado * MedidaLado;
        }

        public decimal CalcularPerimetro()
        {
            return MedidaLado * 4;
        }
    }
}
