﻿using CodingChallenge.Dominio.Idiomas;
using CodingChallenge.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodingChallenge.Dominio.Interfaces
{
    public abstract class IdiomaBase
    {
        public List<CustomMensaje> Mensajes { get; protected set; }

        public string BuscarMensajeEspecifico(TipoMensaje tipoMensaje, string filtro)
        {
            var mensaje = Mensajes.FirstOrDefault(x => x.Tipo == tipoMensaje && x.Mensaje.ToLower().Contains(filtro.ToLower()));
            return mensaje != null ? mensaje.Mensaje : string.Empty;
        }

        public string BuscarMensajeEspecifico(TipoMensaje tipoMensaje)
        {
            var mensaje =  Mensajes.FirstOrDefault(x => x.Tipo == tipoMensaje);
            return mensaje != null ? mensaje.Mensaje : string.Empty;
        }

        public string BuscarMensajeEspecifico(TipoMensaje tipoMensaje, Type tipoClase)
        {
            var mensaje = Mensajes.FirstOrDefault(x => x.Tipo == tipoMensaje && x.TipoClase == tipoClase);
            return mensaje != null ? mensaje.Mensaje : string.Empty;
        }

        public IEnumerable<string> BuscarMensajes(TipoMensaje tipoMensaje)
        {
            return Mensajes.Where(x => x.Tipo == tipoMensaje).Select(x => x.Mensaje).ToList();
        }

        public IEnumerable<string> BuscarMensajes(string filtro)
        {
            return Mensajes.Where(x => x.Mensaje.ToLower().Contains(filtro.ToLower())).Select(x => x.Mensaje).ToList();
        }

        public IEnumerable<string> BuscarMensajes(Type tipoClase)
        {
            return Mensajes.Where(x => x.TipoClase == tipoClase).Select(x => x.Mensaje).ToList();
        }
    }
}
