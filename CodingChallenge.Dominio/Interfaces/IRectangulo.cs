﻿namespace CodingChallenge.Dominio.Interfaces
{
    public interface IRectangulo : IFormaGeometrica
    {
        decimal Base { get; }
        decimal Altura { get; }
    }
}
