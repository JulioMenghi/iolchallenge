﻿namespace CodingChallenge.Dominio.Interfaces
{
    public interface IFormaGeometrica
    {
        decimal CalcularPerimetro();
        decimal CalcularArea();
    }
}
