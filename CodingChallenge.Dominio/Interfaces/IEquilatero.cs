﻿namespace CodingChallenge.Dominio.Interfaces
{
    public interface IEquilatero : IFormaGeometrica
    {
        decimal MedidaLado { get; }
    }
}
