﻿namespace CodingChallenge.Dominio.Interfaces
{
    public interface ITrapecio : IFormaGeometrica
    {
        decimal Altura { get; }
        decimal Base1 { get; }
        decimal Base2 { get; }
        decimal Lado3 { get; }
        decimal Lado4 { get; }
    }
}
